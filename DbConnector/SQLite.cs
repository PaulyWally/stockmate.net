﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;

namespace DbConnector
{
    public class SQLite
    {

        private SQLiteConnection con;

        public string ConnectionString { get; private set; }

        public SQLite(string fqDbFilename)
        {
            ConnectionString = String.Format("Data Source={0};Version=3;", fqDbFilename);
            con = new SQLiteConnection(ConnectionString);
            
        }
        
        public bool RecordsExist(string query)
        {

            bool ret;
            try
            {
                using (SQLiteCommand cmd = new SQLiteCommand(query, con))
                {
                    open();
                    SQLiteDataReader dr = cmd.ExecuteReader();
                    ret = dr.HasRows;
                    dr.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                close();
            }

            return ret;
        }

        public void ExecuteNonQuery(string query)
        {

            throw new Exception("refactor using the method below");
            //using (SQLiteCommand cmd = new SQLiteCommand(query, con))
            //{
            //    open();
            //    cmd.ExecuteNonQuery();
            //    while (con.State == ConnectionState.Executing || con.State == ConnectionState.Fetching)
            //    {
            //        //wait
            //    }
            //    close();
            //}

        }

        public void ExecuteNonQuery(List<string> queries)
        {

            

            using (SQLiteConnection con = new SQLiteConnection(ConnectionString))
            {
                con.Open();

                using (SQLiteTransaction tr = con.BeginTransaction())
                {
                    using (SQLiteCommand cmd = con.CreateCommand())
                    {
                        
                        foreach (string query in queries)
                        {
                            cmd.Transaction = tr;
                            cmd.CommandText = query;
                            cmd.ExecuteNonQuery();
                        }
                            
                    }

                    tr.Commit();
                }

                con.Close();
            }
        }

        public DataTable ExecuteQuery(string query)
        {

            DataTable ret = new DataTable();

            try
            {

                using (SQLiteDataAdapter da = new SQLiteDataAdapter(query, con))
                {
                    open();
                    da.Fill(ret);
                    close();
                }

                return ret;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            
        }

        public int? GetLastInsertIdentity(string tableName)
        {
            int? ret = null;

            string query = String.Format(@"SELECT seq FROM sqlite_sequence WHERE name = '{0}';", tableName);

            using (SQLiteCommand cmd = new SQLiteCommand(query, con))
            {
                open();
                SQLiteDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ret = (int)dr["seq"];
                }
                close();
            }

            return ret;
        }

        private void open()
        {
            if (con.State != System.Data.ConnectionState.Closed)
            {
                close();
            }

            con.Open();
        }

        private void close()
        {
            if (con.State != System.Data.ConnectionState.Closed)
            {

                con.Close();
            }
        }
    }
}
