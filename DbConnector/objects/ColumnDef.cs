﻿

namespace DbConnector.objects
{
    public class ColumnDef
    {
        private DataType dataType;

        public int Index { get; set; }
        public string Name { get; set; }
        public DataType Type
        {
            get
            {
                return dataType;
            }
            set
            {
                setAllDataTypes(value);
            }
        }
        public int Length { get; set; }
        public int Precision { get; set; }
        public string Type_Mssql { get; private set; }
        public System.Type Type_System { get; private set; }

        private void setAllDataTypes(DataType type)
        {
            dataType = type;

            switch (dataType)
            {
                case DataType.BOOLEAN:
                    this.Type_Mssql = "BIT";
                    this.Type_System = typeof(System.Boolean);
                    break;
                case DataType.CURRENCY:
                    this.Type_Mssql = "DECIMAL";
                    this.Type_System = typeof(System.Decimal);
                    break;
                case DataType.DATE:
                    this.Type_Mssql = "DATE";
                    this.Type_System = typeof(System.DateTime);
                    break;
                case DataType.DATETIME:
                    this.Type_Mssql = "DATETIME2";
                    this.Type_System = typeof(System.DateTime);
                    break;
                case DataType.DOUBLE:
                    this.Type_Mssql = "FLOAT";
                    this.Type_System = typeof(System.Double);
                    break;
                case DataType.FLOAT:
                    this.Type_Mssql = "FLOAT";
                    this.Type_System = typeof(System.Double);
                    break;
                case DataType.INT:
                    this.Type_Mssql = "INT";
                    this.Type_System = typeof(System.Int32);
                    break;
                case DataType.LONG:
                    this.Type_Mssql = "BIGINT";
                    this.Type_System = typeof(System.Int64);
                    break;
                case DataType.STRING:
                    this.Type_Mssql = "NVARCHAR";
                    this.Type_System = typeof(System.String);
                    break;
                case DataType.MEMO:
                    this.Type_Mssql = "NTEXT";
                    this.Type_System = typeof(System.String);
                    break;
            }
        }

        public enum DataType
        {
            INT,
            STRING,
            BOOLEAN,
            CURRENCY,
            DATE,
            DATETIME,
            DOUBLE,
            FLOAT,
            LONG,
            MEMO
        }

    }
}
