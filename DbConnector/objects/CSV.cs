﻿using System.IO;


namespace DbConnector.objects
{
    public class CSV
    {

        public string FQFilename { get; set; }
        public string Filename { get; set; }
        public string DestinationName { get; set; }

        public CSV(string fqFilename)
        {
            this.FQFilename = fqFilename;

            FileInfo f = new FileInfo(fqFilename);
            this.Filename = f.Name;
            this.DestinationName = this.Filename;

        }

        public void Add(string fqFilename)
        {

            FileInfo f = new FileInfo(fqFilename);

            this.FQFilename = fqFilename;
            this.Filename = f.Name;

        }

    }
}
