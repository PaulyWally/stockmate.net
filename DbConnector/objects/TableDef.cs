﻿using System.Collections.Generic;

namespace DbConnector.objects
{
    public class TableDef
    {

        public string Name { get; set; }
        public List<ColumnDef> Columns { get; set; }

    }
}
