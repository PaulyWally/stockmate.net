﻿using GenericParsing;
using System;
using System.Collections.Generic;
using System.Data;

namespace DbConnector
{

    public class CSV
    {

        #region =================================== Properties ===================================

        public List<objects.CSV> CSVFiles { get; set; }
        public char ColumnDelimiter { get; set; }
        public char TextQualifier { get; set; }
        public bool HasHeader { get; set; }

        #endregion =================================== Properties ===================================

        public CSV()
        {
            
            this.CSVFiles = new List<objects.CSV>();
            this.ColumnDelimiter = ',';
            this.TextQualifier = '\"';
            this.HasHeader = true;
            
        }

        #region =================================== Public Methods ===================================

        public DataSet GetDataSet()
        {

            DataSet ds = new DataSet();
            
            foreach (DbConnector.objects.CSV csv in this.CSVFiles)
            {

                //FileInfo fi = new FileInfo(file);
                //string tableName = fi.Name;
                //tableName = tableName.Substring(0, tableName.Length - fi.Extension.Length);
                //tableName = tableName.Replace(@".", String.Empty);

                ds.Tables.Add(GetDataTable(csv));

            }

            return ds;


        }

        public DataTable GetDataTable(DbConnector.objects.CSV csv, int maxBufferSize = 131072)
        {

            if (maxBufferSize < 1) maxBufferSize = 131072;

            try
            {
                using (GenericParserAdapter p = new GenericParserAdapter(csv.FQFilename))
                {

                    p.MaxBufferSize = maxBufferSize;
                    p.ColumnDelimiter = this.ColumnDelimiter;

                    p.FirstRowHasHeader = true;
                    p.FirstRowSetsExpectedColumnCount = true;
                    p.TextQualifier = this.TextQualifier;

                    using (DataTable dt = p.GetDataTable())
                    {
                        dt.TableName = csv.DestinationName;
                        return dt;
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }


            //dt.TableName = csv.DestinationName;
            //return dt;

        }

        #endregion =================================== Public Methods ===================================

        #region =================================== Private Methods ===================================

        #endregion =================================== Private Methods ===================================

    }
}
