﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace GroovyTextFileIO
{
    public class XML
    {

        #region =================================== Properties ===================================

        public string FQFileName { get; set; }
        private XmlDocument Contents { get; set; }
        public bool FileExists
        {
            get
            {
                if (File.Exists(this.FQFileName))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        public string NamespaceURI { get; set; }

        #endregion =================================== Properties ===================================

        public XML(string fqFileName)
        {

            this.FQFileName = fqFileName;
            if (File.Exists(this.FQFileName)) loadXmlContentsFromFile();

        }

        #region =================================== Public Methods ===================================

        public void CreateNewXmlDocument(string rootElement)
        {
            XDocument xDoc = new XDocument(new XElement(rootElement));
            xDoc.Save(this.FQFileName);
            loadXmlContentsFromFile();
        }

        public void CreateElement(string elementName, string parentElementPath, string value = null)
        {

            if (String.IsNullOrWhiteSpace(value)) value = String.Empty;

            XmlNode parent = this.Contents.SelectSingleNode(parentElementPath);
            XmlElement child = this.Contents.CreateElement(elementName);
            child.InnerText = value;
            parent.AppendChild(child).InnerText = value;

            Save();

        }

        public bool ElementExists(string nodePath)
        {

            XmlNode parent = this.Contents.SelectSingleNode(nodePath);
            if (parent == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public List<string> GetAttributeValues(string elementPath, string attributeName)
        {

            List<string> ret = new List<string>();
           
            foreach (XmlNode node in getXmlNodeList(elementPath))
            {
                foreach (XmlAttribute attr in node.Attributes)
                {

                    if (attr.Name.Equals(attributeName)) ret.Add(attr.Value);

                }
            }

            return ret;

        }

        public List<string> GetElementInnerText(string elementPath)
        {
            List<string> ret = new List<string>();
            
            foreach (XmlNode node in getXmlNodeList(elementPath))
            {
                ret.Add(node.InnerText);
            }

            return ret;

        }

        public List<string> GetElementInnerText(string elementParentPath, string attributeName, string attributeValue, string childElement)
        {
            List<string> ret = new List<string>();

            string s = elementParentPath + String.Format("[@{0}=\"{1}\"]/{2}", attributeName, attributeValue, childElement);
            
            foreach (XmlNode node in getXmlNodeList(s))
            {
                ret.Add(node.InnerText);
            }

            return ret;

        }

        public void Save()
        {

            this.Contents.Save(this.FQFileName);

        }


        #endregion =================================== Public Methods ===================================


        #region =================================== Private Methods ===================================

        private void loadXmlContentsFromFile()
        {

            this.Contents = new XmlDocument();
            this.Contents.Load(this.FQFileName);

        }

        private XDocument toXDocument(XmlDocument xmlDocument)
        {

            using (var nodeReader = new XmlNodeReader(xmlDocument))
            {
                nodeReader.MoveToContent();
                return XDocument.Load(nodeReader);
            }
            
        }

        private XmlDocument toXmlDocument(XDocument xDocument)
        {

            XmlDocument xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        private XmlNodeList getXmlNodeList(string elementPath)
        {
            XmlNodeList nodes = this.Contents.SelectNodes(elementPath);

            try
            {
                if (String.IsNullOrWhiteSpace(this.NamespaceURI))
                {
                    nodes = this.Contents.SelectNodes(elementPath);
                }
                else
                {
                    XmlNamespaceManager nsm = new XmlNamespaceManager(this.Contents.NameTable);
                    nsm.AddNamespace(@"ns", this.NamespaceURI);
                    elementPath = elementPath.Replace(@"/", @"/ns:");
                    nodes = this.Contents.SelectNodes(elementPath, nsm);
                }

                return nodes;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

        }

        #endregion =================================== Private Methods ===================================

    }
}
