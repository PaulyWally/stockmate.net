﻿using System.Collections.Generic;
using System.IO;

namespace AppSettings
{
    public class Settings
    {
        public FileInfo SettingsFile { get; set; }
        public string AlphaAdvantageBaseUrl { get; set; }
        public string AlphaAdvantageKey { get; private set; }
        public string SQLiteDB { get; private set; }
        public string PriceDataDownload_ResponseSize { get; private set; }
        public bool PriceDataDownload_Daily { get; private set; }
        public bool PriceDataDownload_30min { get; private set; }
        public bool PriceDataDownload_15min { get; private set; }
        public bool PriceDataDownload_5min { get; private set; }
        public bool PriceDataDownload_1min { get; private set; }

        public Settings(string fqFilename)
        {
            this.SettingsFile = new FileInfo(fqFilename);

            PriceDataDownload_Daily = true;
            PriceDataDownload_30min = true;
            PriceDataDownload_15min = true;
            PriceDataDownload_5min = true;
            PriceDataDownload_1min = true;
            
            loadSettingsFromSettingsFile();
        }

        private void loadSettingsFromSettingsFile()
        {
            GroovyTextFileIO.XML settingsFile = new GroovyTextFileIO.XML(this.SettingsFile.FullName);

            List<string> alphaAdvantageBaseUrl = settingsFile.GetAttributeValues(@"/StockMate/AlphaAdvantage", @"BaseUrl");
            List<string> alphaAdvantageKey = settingsFile.GetAttributeValues(@"/StockMate/AlphaAdvantage", @"Key");
            List<string> sqliteDb = settingsFile.GetAttributeValues(@"/StockMate/SQLiteDB", @"Path");
            List<string> priceDataDownload_ResponseSize = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"ResponseSize");
            List<string> priceDataDownload_Daily = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"Daily");
            List<string> priceDataDownload_30min = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"_30min");
            List<string> priceDataDownload_15min = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"_15min");
            List<string> priceDataDownload_5min = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"_5min");
            List<string> priceDataDownload_1min = settingsFile.GetAttributeValues(@"/StockMate/PriceDataDownload", @"_1min");

            if (alphaAdvantageBaseUrl.Count == 1) this.AlphaAdvantageBaseUrl = alphaAdvantageBaseUrl[0].Trim();
            if (alphaAdvantageKey.Count == 1) this.AlphaAdvantageKey = alphaAdvantageKey[0].Trim();
            if (sqliteDb.Count == 1) this.SQLiteDB = sqliteDb[0].Trim();
            if (priceDataDownload_ResponseSize.Count == 1) this.PriceDataDownload_ResponseSize = priceDataDownload_ResponseSize[0].Trim().ToUpper();
            if (priceDataDownload_Daily.Count == 1 && priceDataDownload_Daily[0].Trim().ToUpper() == "FALSE") PriceDataDownload_Daily = false;
            if (priceDataDownload_30min.Count == 1 && priceDataDownload_30min[0].Trim().ToUpper() == "FALSE") PriceDataDownload_30min = false;
            if (priceDataDownload_15min.Count == 1 && priceDataDownload_15min[0].Trim().ToUpper() == "FALSE") PriceDataDownload_15min = false;
            if (priceDataDownload_5min.Count == 1 && priceDataDownload_5min[0].Trim().ToUpper() == "FALSE") PriceDataDownload_5min = false;
            if (priceDataDownload_1min.Count == 1 && priceDataDownload_1min[0].Trim().ToUpper() == "FALSE") PriceDataDownload_1min = false;

        }

        private void throwException(string message)
        {
            throw new System.Exception(message);
        }
    }
}
