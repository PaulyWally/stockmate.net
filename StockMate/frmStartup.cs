﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace StockMate
{
    public partial class frmStartup : Form
    {

        string binDirectory { get; set; }
        AppSettings.Settings appSettings { get; set; }

        public frmStartup(string binDirectory, AppSettings.Settings appSettings)
        {

            this.binDirectory = binDirectory;
            this.appSettings = appSettings;

            InitializeComponent();
        }

        private void btnDownload_Click(object sender, System.EventArgs e)
        {

            try
            {
                StockMateFramework.Data.Api.IApi api = new StockMateFramework.Data.Api.AlphaAdvantage.Connector(appSettings.AlphaAdvantageBaseUrl, appSettings.AlphaAdvantageKey);
                DbConnector.SQLite localDb = new DbConnector.SQLite(appSettings.SQLiteDB);

                StockMateFramework.Data.Etl.MetaData metaData = new StockMateFramework.Data.Etl.MetaData(localDb);
                StockMateFramework.Data.Etl.PriceData stockData = new StockMateFramework.Data.Etl.PriceData(api, localDb, appSettings);

                List<string> symbols = new List<string>();
                if (txtSymbol.Text.Trim().Length > 0)
                {
                    symbols.Add(txtSymbol.Text.Trim().ToUpper());
                }
                else
                {
                    symbols = metaData.GetSymbols();
                }
                   
                foreach (string symbol in symbols)
                {
                    stockData.Download(symbol);
                }

                MessageBox.Show("Download Complete.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }
    }
}
