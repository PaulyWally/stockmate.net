﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

namespace StockMate
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            string binDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            AppSettings.Settings appSettings = new AppSettings.Settings(binDirectory + @"\StockMate.Settings.xml");

            //test01(appSettings);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmStartup(binDirectory, appSettings));

        }

        private static void test01(AppSettings.Settings appSettings)
        {

            StockMateFramework.Data.Api.IApi api = new StockMateFramework.Data.Api.AlphaAdvantage.Connector(appSettings.AlphaAdvantageBaseUrl, appSettings.AlphaAdvantageKey);
            DbConnector.SQLite localDb = new DbConnector.SQLite(appSettings.SQLiteDB);

            //List<StockMateController.model.StockPriceData> data = api.GetPriceData(@"MSFT", StockMateController.Enums.TimeSeries.TIME_SERIES_DAILY, StockMateController.Enums.TimeInterval._5MIN, StockMateController.Enums.ResponseSize.COMPACT);
            //List<StockMateController.model.StockPriceData> data = api.GetPriceData(@"MSFT", StockMateController.Enums.TimeSeries.TIME_SERIES_INTRADAY, StockMateController.Enums.TimeInterval._30min, StockMateController.Enums.ResponseSize.full);

            //StockMateController.Etl etl = new StockMateController.Etl(api, localDb);
            //etl.DownloadPriceData();

            StockMateFramework.Data.Etl.MetaData metaData = new StockMateFramework.Data.Etl.MetaData(localDb);
            StockMateFramework.Data.Etl.PriceData stockData = new StockMateFramework.Data.Etl.PriceData(api, localDb, appSettings);

            List<string> symbols = metaData.GetSymbols();
            foreach (string symbol in symbols)
            {
                stockData.Download(symbol);
            }

        }
    }
}
