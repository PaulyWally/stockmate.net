﻿using StockMateFramework.Model;
using System.Collections.Generic;
using static StockMateFramework.Enums.Api;

namespace StockMateFramework.Data.Api
{
    public interface IApi
    {
        List<StockPriceData> GetPriceData(string symbol, TimeSeries timeSeries,  TimeInterval timeInterval, ResponseSize responseSize);
    }
}
