﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
//using System.Net.Http;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Linq;
using StockMateFramework.Model;
using StockMateFramework.Helper;
using static StockMateFramework.Enums.Api;

namespace StockMateFramework.Data.Api.AlphaAdvantage
{ 
    public class Connector : StockMateFramework.Data.Api.IApi
    {

        private HString hstring { get; set; }

        public string ApiBaseUrl { get; private set; }
        public string ApiKey { get; private set; }
        public string ApiEndpoint { get; private set; }

        public Connector(string baseUrl, string key)
        {
            this.ApiBaseUrl = baseUrl;
            this.ApiKey = key;

            initClass();
        }

        public List<StockPriceData> GetPriceData(string symbol, TimeSeries series, TimeInterval interval, ResponseSize responseSize)
        {


            // Setup the call
            string s = this.hstring.CleanEnum(series.ToString());
            string i = this.hstring.CleanEnum(interval.ToString());

            string call = String.Format(@"query?function={0}&symbol={1}&interval={2}&outputsize={3}", s, symbol, i, responseSize);

            List<StockPriceData> ret = new List<StockPriceData>();

            string r = apiCall(call).Result;
            Response response = JsonConvert.DeserializeObject<Response>(r);

            if (response.TimeSeries_1min != null)
            {
                foreach (JProperty p in response.TimeSeries_1min.Properties())
                {
                    
                    string value = p.Value.ToString();
                    JObject j = JObject.Parse(value);
                    PriceData q = JsonConvert.DeserializeObject<PriceData>(value);

                    ret.Add(getStockPriceData(q, p.Name, response.MetaData.Symbol, response.MetaData.TimeZone));

                }
            }

            if (response.TimeSeries_5min != null)
            {
                foreach (JProperty p in response.TimeSeries_5min.Properties())
                {
                    string value = p.Value.ToString();
                    JObject j = JObject.Parse(value);
                    PriceData q = JsonConvert.DeserializeObject<PriceData>(value);

                    ret.Add(getStockPriceData(q, p.Name, response.MetaData.Symbol, response.MetaData.TimeZone));
                }
            }

            if (response.TimeSeries_15min != null)
            {
                foreach (JProperty p in response.TimeSeries_15min.Properties())
                {
                    string value = p.Value.ToString();
                    JObject j = JObject.Parse(value);
                    PriceData q = JsonConvert.DeserializeObject<PriceData>(value);

                    ret.Add(getStockPriceData(q, p.Name, response.MetaData.Symbol, response.MetaData.TimeZone));
                }
            }

            if (response.TimeSeries_30min != null)
            {
                foreach (JProperty p in response.TimeSeries_30min.Properties())
                {
                    string value = p.Value.ToString();
                    JObject j = JObject.Parse(value);
                    PriceData q = JsonConvert.DeserializeObject<PriceData>(value);

                    ret.Add(getStockPriceData(q, p.Name, response.MetaData.Symbol, response.MetaData.TimeZone));
                }
            }

            if (response.TimeSeries_Daily != null)
            {
                foreach (JProperty p in response.TimeSeries_Daily.Properties())
                {
                    string value = p.Value.ToString();
                    JObject j = JObject.Parse(value);
                    PriceData q = JsonConvert.DeserializeObject<PriceData>(value);

                    ret.Add(getStockPriceData(q, p.Name, response.MetaData.Symbol, response.MetaData.TimeZone));
                }
            }

            return ret;

        }
        
        private void initClass()
        {
            this.hstring = new HString();
        }

        private async Task<string> apiCall(string call)
        {

            string uri = String.Format(@"{0}{1}{2}&apikey={3}", ApiBaseUrl, ApiEndpoint, call, ApiKey);
            //uri = @"https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=MSFT&interval=5min&apikey=demo";

            string ret = string.Empty;
            using (System.Net.Http.HttpClient client = new System.Net.Http.HttpClient())
            {
                ret = await client.GetStringAsync(uri).ConfigureAwait(false);
            }

            return normalizeResponse(ret);

        }

        private string normalizeResponse(string response)
        {
            //  \\\"\\d\\.\\s\\S+\\\"\\:
            //  \\\"\\d\\.\\s[a-zA-Z\s]+\\\"\\:
            string regex = "\\\"\\d\\.\\s[a-zA-Z\\s]+\\\"\\:";
            Regex regex2 = new Regex(regex);

            //MatchCollection matches = Regex.Matches(response, regex).OfType<Match>().Select(m => m.Groups[0].Value).Distinct());
            string[] matches = regex2.Matches(response).Cast<Match>().Select(m => m.Value).Distinct().OrderBy(s => s).ToArray<string>();

            foreach (string s in matches)
            {
                string replacement = Regex.Replace(s, "\\d\\.\\s","");
                response = response.Replace(s, replacement);
            }

            return response;
        }

        private StockPriceData getStockPriceData(PriceData priceData, string priceDateTime, string symbol, string timeZone)
        {
            StockPriceData ret = new StockPriceData();

            Match regexMatch = Regex.Match(priceDateTime, @"^\d{4}\-\d{2}\-\d{2}$", RegexOptions.Multiline);
            if (regexMatch.Success) priceDateTime += @" 00:00:00";

            ret.DateAndTime = DateTime.ParseExact(priceDateTime, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            ret.Close = decimal.Parse(priceData.Close);
            ret.High = decimal.Parse(priceData.High);
            ret.Low = decimal.Parse(priceData.Low);
            ret.Open = decimal.Parse(priceData.Open);
            ret.Symbol = symbol;
            ret.TimeZone = timeZone;
            ret.Volume = long.Parse(priceData.Volume);

            return ret;

        }

    }
}
