﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace StockMateFramework.Data.Api.AlphaAdvantage
{
    public class Response
    {

        [JsonProperty("Meta Data")]
        public MetaData MetaData { get; set; }

        [JsonProperty("Time Series (1min)")]
        //public TimeSeries5min_WithSpaces TimeSeries5min_WithSpaces { get; set; }
        public JObject TimeSeries_1min { get; set; }

        [JsonProperty("Time Series (5min)")]
        //public TimeSeries5min_WithSpaces TimeSeries5min_WithSpaces { get; set; }
        public JObject TimeSeries_5min { get; set; }

        [JsonProperty("Time Series (15min)")]
        //public TimeSeries5min_WithSpaces TimeSeries5min_WithSpaces { get; set; }
        public JObject TimeSeries_15min { get; set; }

        [JsonProperty("Time Series (30min)")]
        //public TimeSeries5min_WithSpaces TimeSeries5min_WithSpaces { get; set; }
        public JObject TimeSeries_30min { get; set; }
        
        [JsonProperty("Time Series (Daily)")]
        //public TimeSeries5min_WithSpaces TimeSeries5min_WithSpaces { get; set; }
        public JObject TimeSeries_Daily { get; set; }
    }
}
