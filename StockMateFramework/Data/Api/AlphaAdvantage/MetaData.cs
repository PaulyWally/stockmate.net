﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Data.Api.AlphaAdvantage
{
    public class MetaData
    {
        [Newtonsoft.Json.JsonProperty("Information")]
        public string Information { get; set; }

        [Newtonsoft.Json.JsonProperty("Symbol")]
        public string Symbol { get; set; }

        [Newtonsoft.Json.JsonProperty("Last Refreshed")]
        public string LastRefreshed { get; set; }

        [Newtonsoft.Json.JsonProperty("Interval")]
        public string Interval { get; set; }

        [Newtonsoft.Json.JsonProperty("Output Size")]
        public string OutputSize { get; set; }

        [Newtonsoft.Json.JsonProperty("Time Zone")]
        public string TimeZone { get; set; }
    }
}
