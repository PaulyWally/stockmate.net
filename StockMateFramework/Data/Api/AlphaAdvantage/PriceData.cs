﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Data.Api.AlphaAdvantage
{
    public class PriceData
    {
        [Newtonsoft.Json.JsonProperty("open")]
        public string Open { get; set; }

        [Newtonsoft.Json.JsonProperty("high")]
        public string High { get; set; }

        [Newtonsoft.Json.JsonProperty("low")]
        public string Low { get; set; }

        [Newtonsoft.Json.JsonProperty("close")]
        public string Close { get; set; }

        [Newtonsoft.Json.JsonProperty("volume")]
        public string Volume { get; set; }
    }
}
