﻿using StockMateFramework.Model;
using System;
using System.Collections.Generic;
using System.Data;
using StockMateFramework.Data.Api;

namespace StockMateFramework.Data.Etl
{
    public class PriceData
    {

        public IApi Source { get; set; }

        public DbConnector.SQLite Dest { get; set; }
        private AppSettings.Settings appSettings { get; set; }

        public PriceData(Api.IApi source, DbConnector.SQLite dest, AppSettings.Settings appSettings)
        {
            this.Source = source;
            this.Dest = dest;
            this.appSettings = appSettings;
        }

        public void Download(string symbol)
        {

            Enums.Api.ResponseSize responseSize;
            switch(appSettings.PriceDataDownload_ResponseSize)
            {
                case @"FULL":
                    responseSize = Enums.Api.ResponseSize.full;
                    break;
                case @"COMPACT":
                    responseSize = Enums.Api.ResponseSize.compact;
                    break;
                default:
                    responseSize = Enums.Api.ResponseSize.full;
                    break;
            }

            int fk_Security = getPk_Security(symbol);
            List<string> queries = new List<string>();

            if (appSettings.PriceDataDownload_Daily)
            {
                List<StockPriceData> priceData = Source.GetPriceData(symbol, Enums.Api.TimeSeries.TIME_SERIES_DAILY, Enums.Api.TimeInterval._1min, responseSize);
                foreach (StockPriceData priceDate in priceData)
                {
                    int fk_TimeZone = getPk_TimeZone(priceDate.TimeZone);
                    string priceDateAndTime = priceDate.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss");
                    //if (!Dest.RecordsExist(String.Format(@"SELECT 1 FROM price_data WHERE fk_security = {0} AND price_date = '{1}' AND fk_price_date_time_zone = {2};", fk_Security, priceDateAndTime, fk_TimeZone)))
                    {
                        //Dest.ExecuteNonQuery(String.Format(@"INSERT INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                        queries.Add(String.Format(@"INSERT OR IGNORE INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));

                    }

                }
            }

            if (appSettings.PriceDataDownload_30min)
            {
                List<StockPriceData> priceData = Source.GetPriceData(symbol, Enums.Api.TimeSeries.TIME_SERIES_INTRADAY, Enums.Api.TimeInterval._30min, responseSize);
                foreach (StockPriceData priceDate in priceData)
                {
                    int fk_TimeZone = getPk_TimeZone(priceDate.TimeZone);
                    string priceDateAndTime = priceDate.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss");
                    //if (!Dest.RecordsExist(String.Format(@"SELECT 1 FROM price_data WHERE fk_security = {0} AND price_date = '{1}' AND fk_price_date_time_zone = {2};", fk_Security, priceDateAndTime, fk_TimeZone)))
                    {
                        //Dest.ExecuteNonQuery(String.Format(@"INSERT INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                        queries.Add(String.Format(@"INSERT OR IGNORE INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                    }

                }
            }

            if (appSettings.PriceDataDownload_15min)
            {
                List<StockPriceData> priceData = Source.GetPriceData(symbol, Enums.Api.TimeSeries.TIME_SERIES_INTRADAY, Enums.Api.TimeInterval._15min, responseSize);
                foreach (StockPriceData priceDate in priceData)
                {
                    int fk_TimeZone = getPk_TimeZone(priceDate.TimeZone);
                    string priceDateAndTime = priceDate.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss");
                    //if (!Dest.RecordsExist(String.Format(@"SELECT 1 FROM price_data WHERE fk_security = {0} AND price_date = '{1}' AND fk_price_date_time_zone = {2};", fk_Security, priceDateAndTime, fk_TimeZone)))
                    {
                        //Dest.ExecuteNonQuery(String.Format(@"INSERT INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                        queries.Add(String.Format(@"INSERT OR IGNORE INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                    }

                }
            }

            if (appSettings.PriceDataDownload_5min)
            {
                List<StockPriceData> priceData = Source.GetPriceData(symbol, Enums.Api.TimeSeries.TIME_SERIES_INTRADAY, Enums.Api.TimeInterval._5min, responseSize);
                foreach (StockPriceData priceDate in priceData)
                {
                    int fk_TimeZone = getPk_TimeZone(priceDate.TimeZone);
                    string priceDateAndTime = priceDate.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss");
                    //if (!Dest.RecordsExist(String.Format(@"SELECT 1 FROM price_data WHERE fk_security = {0} AND price_date = '{1}' AND fk_price_date_time_zone = {2};", fk_Security, priceDateAndTime, fk_TimeZone)))
                    {
                        //Dest.ExecuteNonQuery(String.Format(@"INSERT INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                        queries.Add(String.Format(@"INSERT OR IGNORE INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                    }

                }
            }

            if (appSettings.PriceDataDownload_1min)
            {
                List<StockPriceData> priceData = Source.GetPriceData(symbol, Enums.Api.TimeSeries.TIME_SERIES_INTRADAY, Enums.Api.TimeInterval._1min, responseSize);
                foreach (StockPriceData priceDate in priceData)
                {
                    int fk_TimeZone = getPk_TimeZone(priceDate.TimeZone);
                    string priceDateAndTime = priceDate.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss");
                    //if (!Dest.RecordsExist(String.Format(@"SELECT 1 FROM price_data WHERE fk_security = {0} AND price_date = '{1}' AND fk_price_date_time_zone = {2};", fk_Security, priceDateAndTime, fk_TimeZone)))
                    {
                        //Dest.ExecuteNonQuery(String.Format(@"INSERT INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                        queries.Add(String.Format(@"INSERT OR IGNORE INTO price_data (fk_security, price_date, fk_price_date_time_zone, open, high, low, close, volume) VALUES ({0},'{1}',{2},{3},{4},{5},{6},{7});", fk_Security, priceDateAndTime, fk_TimeZone, priceDate.Open, priceDate.High, priceDate.Low, priceDate.Close, priceDate.Volume));
                    }

                }
            }
            Dest.ExecuteNonQuery(queries);
        }

        private int getPk_TimeZone(string timeZone)
        {
            string pk_Enum_Type_Time_Zone = Dest.ExecuteQuery(@"SELECT pk FROM enum_types WHERE name = 'Time Zone' LIMIT 1;").Rows[0][0].ToString();
            string pk_Time_Zone = Dest.ExecuteQuery(String.Format(@"SELECT pk FROM enums WHERE name = '{0}' AND fk_enum_type = {1} LIMIT 1;", timeZone, pk_Enum_Type_Time_Zone)).Rows[0][0].ToString();

            return int.Parse(pk_Time_Zone);
        }

        private int getPk_Security(string symbol)
        {
            string pk_Security = Dest.ExecuteQuery(String.Format(@"SELECT pk FROM securities WHERE symbol = '{0}' LIMIT 1;", symbol)).Rows[0][0].ToString();

            return int.Parse(pk_Security);
        }

    }

}
