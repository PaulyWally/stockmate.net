﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbConnector;

namespace StockMateFramework.Data.Etl
{
    public class MetaData
    {

        public SQLite LocalDb { get; set; }

        public MetaData(SQLite localDb)
        {
            this.LocalDb = localDb;
        }

        public List<string> GetSymbols()
        {

            List<string> ret = new List<string>();

            using (DataTable dt = LocalDb.ExecuteQuery(@"SELECT DISTINCT symbol FROM securities ORDER BY pk ASC;"))
            {
                foreach (DataRow row in dt.Rows)
                {
                    ret.Add(row[0].ToString());
                }
            }

                return ret;

        }

    }
}
