﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Model
{
    public class StockPriceData : StockMetaData
    {
        public DateTime DateAndTime { get; set; }
        public Decimal Open { get; set; }
        public Decimal Close { get; set; }
        public Decimal High { get; set; }
        public Decimal Low { get; set; }
        public Int64 Volume { get; set; }

        public StockPriceData()
        {

        }

    }
}
