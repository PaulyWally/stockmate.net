﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Model
{
    public class StockMetaData
    {
        public string Symbol { get; set; }
        public string TimeZone { get; set; }

        public StockMetaData()
        {

        }

    }
}
