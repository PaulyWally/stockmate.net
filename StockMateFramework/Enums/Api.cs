﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Enums
{
    public class Api
    {
        public enum ResponseSize
        {
            compact,
            full
        }

        public enum TimeSeries
        {
            TIME_SERIES_INTRADAY,
            TIME_SERIES_DAILY
        }

        public enum TimeInterval
        {
            _1min,
            _5min,
            _15min,
            _30min
        }

    }
}
