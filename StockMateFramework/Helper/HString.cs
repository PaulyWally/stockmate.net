﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StockMateFramework.Helper
{
    public class HString
    {

        public HString()
        {

        }

        public string CleanEnum(string value)
        {

            value = value.StartsWith(@"_") ? value.Replace(@"_", @"") : value;

            return value;

        }

    }
}
